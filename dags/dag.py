from datetime import timedelta, datetime
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1)
}

dag = DAG(
    'test_dag',
    default_args=default_args,
    description='Test DAG with Spark',
    start_date=datetime(2023, 10, 11),
    catchup=False
)

# dag_instance()
spark_job = SparkSubmitOperator(task_id='test',
                                application='/opt/airflow/spark/spark_app.py',
                                name='test',
                                conn_id='spark_local',
                                dag=dag
                                )

spark_job