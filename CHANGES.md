### Lab 1
Docker compose + Airflow

Custom DAG with text file processing

### Lab 2

Docker compose + Airflow + Spark

- Spark connection is added
- New volume for spark
- DAG job passed to SparkOperator


### Lab 3

Docker compose + Airflow + Spark + CI/CD

- docker-compose-runner.yaml added
- .gitlab-ci/yml aded
    - test_job added
    - rules added
- Use runner only with tagged jobs


### Lab 4

Loki + Zabbix + Grafana

- Loki, Zabbix, Grafana images added in `docker-compose.yaml`
- Zabbix set
- Grafana connected