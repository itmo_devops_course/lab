# devops-lab

- Docker compose
- Airflow + custom DAG
- Spark
- Loki + Zabbix + Grafana
- CI/CD

## DAG
[DAG](/dags/dag.py) extracts text from `data/input_text`, transforms it and saves into `data/output_text`

## Run Airflow

``` sh
cd docker-compose.yaml path
docker compose up -d
```

## Connect Spark

Visit `localhost:8080` to use airflow web server, then open *Admin -> Connections -> +New*

Use connection settings from below

|            |                      |
| ---        | ---                  |
| Connection | Id spark_local       |
| Connection | Type                 |
| Host       | spark://spark-master |
| Port       | 7077                 |
|            |                      |

Now run the DAG and check `localhost:4040` for Spark workers info

## Set Zabbix

Open `localhost:8082` with `Admin`/`zabbix` credendtials

- Data collection → Templates → Import → airflow_template.yaml
- Data collection → Hosts → airflow webserver → Create host

|             |                                       |
| ---         | ---                                   |
| Host name   | Your airflow webserver container name |
| Templates   | Airflow by HTTP                       |
| Host groups | Applications                          |
|             |                                       |

- Monitoring → Latest data

## Connect Grafana

Execute

``` sh
docker exec -it //container_grafana_id// bash -c "grafana cli plugins install alexanderzobnin-zabbix-app"

docker restart //container_grafana_id//
```

Proceed to `localhost:3000`

- Connections → Data sources → Loki → `http://loki:3100` → Save & Test
- Connections → Data sources → Zabbix → `http://*YOUR_ZABBIX_FRONTEND_CONTAINER_NAME*:8080/api_jsonrpc.php` → Save & Test
- Create Dashbords as you prefer

![Dashboard example](/img/grafana.PNG)