import rules_dict
import re

from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf = SparkConf().setAppName('PySpark App').setMaster('spark://spark-master:7077')
sc = SparkContext(conf=conf)

spark = SparkSession.builder.config(conf=conf).getOrCreate()

def replace(data):
    # Loading REGEX
    rules = rules_dict.get_rules()
    # Processing changes
    for key, val in rules.items():
        data = re.sub(key, val, data)
    return data

def extract():
    with open("/opt/airflow/data/input_text.txt", encoding="utf-8") as file:
        return file.readlines()

def transform(text_data):
    converted_data = []
    for line in text_data:
        converted_data.append(replace(line))

    return converted_data

def load(converted_data):
    with open("/opt/airflow/data/output_text.txt", "w", encoding="utf-8") as file:
        for line in converted_data:
            file.write(line)


text_data = extract()
converted_data = transform(text_data)
load(converted_data)

spark.stop()